import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoList from './components/todo-list/TodoList';
import TodoItem from './components/todo-item/TodoItem';

class App extends Component {
  constructor(){
    super();
    this.state = {
      items: [],
      currentItem: {text:'', key:''}
    }
  }

  handleInput = e => {
    const itemText = e.target.value;
    const currentItem = { text: itemText, key: Date.now() };
    this.setState({
      currentItem,
    });
  }

  addItem = e => {
    e.nativeEvent.preventDefault();
    const newItem = this.state.currentItem;
    if(newItem.itemText !== ''){
      const its = this.state.items;
      its.push(newItem);
      this.setState({
        item: its,
        currentItem: {text: '', key: ''}
      });
    }
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <div className="App-body">
          <TodoList addItem={this.addItem}
            inputElement={this.inputElement}
            handleInput={this.handleInput}
            currentItem={this.state.currentItem}/>
          <div>
          <br></br>
            <TodoItem entries={this.state.items}/>
          </div>

        </div>
      </div>
    );
  }
}

export default App;
