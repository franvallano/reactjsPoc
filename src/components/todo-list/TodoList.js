import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import 'typeface-roboto';

class TodoList extends Component {

  render() {
    return (
      <div className="todoListMain">
        <div className="header">
          <form onSubmit={this.props.addItem}>
            <Input placeholder="Task"
                    value={this.props.currentItem.text}
                    onChange={this.props.handleInput}/>
            <Button variant="contained" color="primary" type="submit">Add</Button>
          </form>
        </div>
      </div>
    )
  }
}

export default TodoList;
